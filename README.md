# 喵易购电子商城项目 

#### 介绍
* 项目地址: http://shop.lovesiqi.ren
* 账号：admin
* 密码：113

#### 预览
##### 基于Element UI 实现的后台管理页面
![输入图片说明](https://images.gitee.com/uploads/images/2021/0825/111938_fbc2f2ef_7435760.png "后台.png")
##### 喵易购商城首页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0825/112437_b81bc171_7435760.png "2021-08-25_112405.png")
##### css动画登录页面
![输入图片说明](https://images.gitee.com/uploads/images/2021/0825/112024_aba7997a_7435760.png "登录.png")

#### 使用说明
* yarn serve
* dotnet run
 
 
#### 环境和依赖
* yarn
* less
* webpack
* eslint
* @vue/cli ~3
* [Echarts](https://echarts.apache.org/zh/index.html) 数据可视化图表
